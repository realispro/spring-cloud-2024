package lab.book.controller;

import lab.book.model.Book;
import lab.book.service.BookService;
import lab.book.service.InventoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@Slf4j
public class BookController {

    private final BookService bookService;
    private final InventoryService inventoryService;


    @Value("${log.prefix:'default'}")
    private String logPrefix;

    @GetMapping("/books")
    List<Book> getBooks() {
        log.info("[" + logPrefix + "] retrieving books");
        return bookService.getBooks();
    }

    @GetMapping("/books/{id}")
    ResponseEntity<Book> getBookById(@PathVariable("id") int id) {
        log.info("[" + logPrefix + "] retrieving book {}", id);
        return ResponseEntity.of(Optional.ofNullable(bookService.getBookById(id)));
    }

    @PostMapping("/books")
    ResponseEntity<Book> addBook(@RequestBody Book book) {
        log.info("[" + logPrefix + "] adding book {}", book);

        book = bookService.registerNewBook(book);
        return ResponseEntity.status(HttpStatus.CREATED).body(book);
    }

    @PatchMapping("/inventory/{id}")
    ResponseEntity<Integer> increaseQuantity(@PathVariable("id") int id, @RequestBody int quantity) {
        log.info("[" + logPrefix + "] adding {} copies of a book {}", quantity, id);
        inventoryService.increaseQuantity(id, quantity);

        return ResponseEntity.accepted().body(bookService.getBookById(id).getQuantity());
    }

    @GetMapping("/inventory/{id}")
    ResponseEntity<Integer> increaseQuantity(@PathVariable("id") int id) {
        log.info("[" + logPrefix + "] checking quantity of book {}",  id);
        return ResponseEntity.accepted().body(bookService.getBookById(id).getQuantity());
    }

    @ExceptionHandler(RuntimeException.class)
    ResponseEntity<String> handleRuntimeException(RuntimeException e){
        log.error("handling runtime exception", e);
        return ResponseEntity.badRequest().body(e.getMessage());
    }


}
