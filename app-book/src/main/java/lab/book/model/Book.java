package lab.book.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;


@Data
@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String title;
    @ManyToOne
    private Author author;
    private String cover;
    private float price;
    private String publisherId;
    private Double averageRating;
    @JsonIgnore
    private int quantity;
}
