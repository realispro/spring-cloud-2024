package lab.book.service;

import lab.book.dao.BookRepository;
import lab.book.model.Book;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class InventoryService {

    private final BookRepository bookRepository;

    public boolean isBookAvailable(int bookId, int quantity){
        log.info("checking availability of a book {} on quantity {}", bookId, quantity);
        return bookRepository.findById(bookId).orElseThrow().getQuantity() > quantity;
    }

    public void reduceStock(int bookId, int quantity){
        log.info("reducing availability of a book {} on quantity {}", bookId, quantity);
        if(isBookAvailable(bookId, quantity)){
            Book book = bookRepository.findById(bookId).orElseThrow();
            book.setQuantity(book.getQuantity()-quantity);
        }
    }

    @Transactional
    public void increaseQuantity(int bookId, int quantityDifff){
        log.info("increasing quantity of book {} by {}", bookId, quantityDifff);
        Book book = bookRepository.findById(bookId).orElseThrow();
        book.setQuantity(book.getQuantity()+quantityDifff);
    }
}
