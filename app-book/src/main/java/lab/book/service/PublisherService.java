package lab.book.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name="app-publisher", fallback = PublisherServiceFallback.class)
public interface PublisherService {

    @GetMapping("/publishers/{publisherId}")
    PublisherDto getPublisherById(@PathVariable String publisherId, @RequestHeader("custom-header") String customHeader);

}
