/*
package lab.book.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

//@Component
@Slf4j
@RequiredArgsConstructor
public class RestPublisherService implements PublisherService {

    private final RestTemplate restTemplate;

    @Override
    @io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker(name="app-publisher", fallbackMethod = "getPublisherByIdFallback")
    public PublisherDto getPublisherById(String publisherId) {

        log.info("checking existence of a publisher {}", publisherId);
        PublisherDto publisher = restTemplate.exchange(
                        "http://app-publisher" + "/publishers/" + publisherId,
                        HttpMethod.GET,
                        HttpEntity.EMPTY,
                        PublisherDto.class).getBody();
        return publisher;
    }

    private PublisherDto getPublisherByIdFallback(String publisherId, Throwable t){
        log.error("fallback method", t);
        return null;
    }
}
*/
