package lab.book.service;

import lab.book.dao.BookRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Slf4j
@RequiredArgsConstructor
public class RatingUpdateListener {

    private final BookRepository bookRepository;

    @KafkaListener(topics = "rating", properties = {"spring.json.value.default.type=lab.book.service.RatingUpdateListener.RatingUpdate"} )
    @Transactional
    public void updateBookRating(RatingUpdate ratingUpdate) {
        log.info("rating update notification received: {}", ratingUpdate);
        bookRepository.findById(ratingUpdate.getBookId()).orElseThrow()
                .setAverageRating(ratingUpdate.averageRate);
    }

    @Data
    static class RatingUpdate{
        private int bookId;
        private double averageRate;
    }
}
