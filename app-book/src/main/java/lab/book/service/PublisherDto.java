package lab.book.service;

import lombok.Data;

@Data
public class PublisherDto {
    private String id;
    private String name;
    private String logo;
}
