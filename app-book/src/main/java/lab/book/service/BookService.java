package lab.book.service;

import lab.book.dao.BookRepository;
import lab.book.model.Book;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class BookService {

    private final BookRepository bookRepository;
    private final PublisherService publisherService;

    public List<Book> getBooks(){
        log.info("querying books");
        return bookRepository.findAll();
    }

    public List<Book> getBookByAuthor(String authorLastName){
        log.info("querying books by author: {}", authorLastName);
        return bookRepository.findAllByAuthorLastName(authorLastName);
    }

    public Book getBookById(int bookId){
        log.info("querying book {}", bookId);
        return bookRepository.findById(bookId).orElse(null);
    }

    public Book registerNewBook(Book book){
        log.info("registering book {}", book);
        if(publisherService.getPublisherById(book.getPublisherId(), "just checking")==null){
            throw new RuntimeException("Publisher not found");
        }
        return bookRepository.save(book);
    }




}
