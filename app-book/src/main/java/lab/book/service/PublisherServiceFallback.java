package lab.book.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PublisherServiceFallback implements PublisherService{
    @Override
    public PublisherDto getPublisherById(String publisherId, String customHeader) {
        log.error("fallback method for getting publisher {} using custom header {}", publisherId, customHeader);
        return null;
    }
}
