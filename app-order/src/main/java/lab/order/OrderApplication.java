package lab.order;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.function.Consumer;
import java.util.function.Function;

@SpringBootApplication
@Slf4j
public class OrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }

    @Bean
    Consumer<String> registerOrder(){
        return orderString -> {
            log.info("Registering new order: {}", orderString);
        };
    }

    @Bean
    Function<String, String> confirmAvailability(){
        return orderString ->{
            // TODO persist order
            log.info("Confirm availability: {}", orderString);
            return orderString;
        };
    }
}
