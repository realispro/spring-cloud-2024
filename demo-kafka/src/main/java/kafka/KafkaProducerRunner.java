package kafka;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RoundRobinPartitioner;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Properties;
import java.util.Random;

//@Component
@Order(2)
@Slf4j
public class KafkaProducerRunner implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        Properties producerProperties = new Properties();
        producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:8097");
        producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        KafkaProducer<String, String> producer = new KafkaProducer<>(producerProperties);

        for (int i = 11; i <= 20; i++) {
            ProducerRecord<String, String> record = new ProducerRecord<>("topic1", String.valueOf(i));
            producer.send(record, (recordMetadata, e) -> {
                // executes every time a record is sent or Exception is thrown
                if (e == null) {
                    // record was successfully sent
                    log.info("metaData: " +
                            "topic " + recordMetadata.topic() +
                            " Offset " + recordMetadata.offset() +
                            " TimeStamp " + recordMetadata.timestamp() +
                            " Partition " + recordMetadata.partition());
                } else {
                    log.error(e.toString());
                }
            });
            log.info("record published: {}", record);
        }

        producer.close();
    }
}
