package kafka;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.Admin;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.ListTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Properties;

//@Component
@Order(1)
@Slf4j
public class KafkaAdminRunner implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        Properties adminProperties = new Properties();
        adminProperties.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:8097");
        Admin admin = Admin.create(adminProperties);

        //admin.createTopics(Collections.singleton(new NewTopic("topic1", 4, (short) 2)));

        ListTopicsResult result = admin.listTopics();
        result.namesToListings().get().entrySet().forEach(e->log.info("naming: {}, listing: {}", e.getKey(), e.getValue()));
        //admin.deleteTopics(List.of("topic1"));

        admin.close();
    }
}
