CREATE TABLE BOOK
(
    ID           INT PRIMARY KEY AUTO_INCREMENT,
    TITLE        TEXT,
    AUTHOR_ID    INT REFERENCES AUTHOR,
    COVER        TEXT    NOT NULL,
    PRICE        INTEGER NOT NULL,
    QUANTITY    INT,
    AVERAGE_RATING FLOAT,
    PUBLISHER_ID TEXT
);

CREATE TABLE AUTHOR
(
    ID         INT PRIMARY KEY AUTO_INCREMENT,
    FIRST_NAME TEXT,
    LAST_NAME  TEXT
);

INSERT INTO AUTHOR(ID, FIRST_NAME, LAST_NAME)
VALUES (1, 'Marko', 'Gargenta'),
       (2, 'Thomas', 'Nield'),
       (3, 'Tom', 'Marrs'),
       (4, 'Dawid', 'Borycki'),
       (5, 'Gary', 'McLean Hall'),
       (6, 'Ian', 'Goodfellow'),
       (7, 'George', 'Stiny'),
       (8, 'Paul', 'Dourish'),
       (9, 'Craig', 'Walls'),
       (10, 'Magnus', 'Larsson');



INSERT INTO BOOK(ID, TITLE, AUTHOR_ID, COVER, PRICE, QUANTITY, PUBLISHER_ID)
VALUES (1, 'Learning Android', 1,
        'https://books.google.pl/books/content?id=bI8jnwEACAAJ&printsec=frontcover&img=1&zoom=1',
        59, 3, '1'),
       (2, 'Getting started with SQL', 2,
        'https://d8mkdcmng3.imgix.net/fd19/books-getting-started-with-sql.jpg?auto=format&bg=0FFF&fit=fill&h=600&q=100&w=600&s=61f1d3e34249222c0c95716a93e0b9c9',
        103, 1, '1'),
       (3, 'JBoss at Work: A Practical Guide', 3, 'https://learning.oreilly.com/library/cover/0596007345/250w/',
        49, 0, '1'),
       (4, 'Programming for the Internet of Things: Using Windows 10 IoT Core and Azure IoT Suite', 4,
        'https://i5.walmartimages.com/asr/6c8411bf-5615-429c-b9ab-3ee8171bafc6_1.f9dd41ce5641a78c2ae3b0b7240f83d7.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF',
        66, 2, '2'),
       (5, 'Adaptive Code', 5,
        'https://d8mkdcmng3.imgix.net/fd19/books-getting-started-with-sql.jpg?auto=format&bg=0FFF&fit=fill&h=600&q=100&w=600&s=61f1d3e34249222c0c95716a93e0b9c9',
        55, 1, '2'),
       (6, 'Deep Learning', 6,
        'https://mitpress.mit.edu/sites/default/files/styles/large_book_cover/http/mitp-content-server.mit.edu%3A18180/books/covers/cover/%3Fcollid%3Dbooks_covers_0%26isbn%3D9780262035613%26type%3D.jpg?itok=opJUfdCT',
        88, 3, '3'),
       (7, 'Shape, Talking about Seeing and Doing', 7,
        'https://architecture.mit.edu/sites/architecture.mit.edu/files/styles/banner_image/public/stiny_shape.png?itok=o7g4LPyQ',
        33, 0, '3'),
       (8, 'The Stuff of bits', 8,
        'https://mitpress.mit.edu/sites/default/files/styles/large_book_cover/http/mitp-content-server.mit.edu%3A18180/books/covers/cover/%3Fcollid%3Dbooks_covers_0%26isbn%3D9780262036207%26type%3D.jpg?itok=cbKAV9Z_',
        22, 11, '3'),
       (9, 'Spring Boot in action', 9,
        'https://m.media-amazon.com/images/I/71rQMKhj+ZL._SY466_.jpg',
        77, 3, '1'),
       (10, 'Microservices with Spring Boot 3 and Spring Cloud', 10,
        'https://m.media-amazon.com/images/I/613JjAL0IZL._SY385_.jpg',
        22, 5, '4');


create table users
(
    id        int primary key auto_increment,
    user_name varchar(255) not null,
    password  varchar(255) not null
);

create table roles
(
    id        int primary key auto_increment,
    user_name varchar(255) not null,
    role_name varchar(255) not null
);

insert into users
values (1, 'dbuser1', 'dbuser1');
insert into users
values (2, 'dbuser2', 'dbuser2');

insert into roles
values (1, 'dbuser1', 'Customer');
insert into roles
values (2, 'dbuser2', 'Regular');