db.createUser({
    user: 'admin',
    pwd: 'admin',
    roles: [
        {
            role: 'readWrite',
            db: 'lab',
        },
    ],
});

db.createCollection('rating', {capped: true, size: 100000});

db.createCollection('publisher', {capped: true, size: 100000});

db.publisher.insert([
    {
        "_id": "1",
        "name": "MIT Press",
        "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/MIT_Press_logo_%28black%29.svg/800px-MIT_Press_logo_%28black%29.svg.png"
    },
    {
        "_id": "2",
        "name": "O'Reilly",
        "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/MIT_Press_logo_%28black%29.svg/800px-MIT_Press_logo_%28black%29.svg.png"
    },
    {
        "_id": "3",
        "name": "Microsoft Press",
        "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Microsoft_press.png/330px-Microsoft_press.png"
    },
    {
        "_id": "4",
        "name": "Packt Publishing",
        "logo": "https://www.packtpub.com/images/logo-new.svg"
    }
])