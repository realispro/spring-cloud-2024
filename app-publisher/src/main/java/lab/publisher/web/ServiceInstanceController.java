package lab.publisher.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
public class ServiceInstanceController {

    private final DiscoveryClient discoveryClient;

    @GetMapping("/services")
    public List<String> getServices(){
        log.info("about to retrieve all services");
        return discoveryClient.getServices();
    }

    @GetMapping("/instances/{id}")
    public List<ServiceInstance> getPublishers(@PathVariable("id") String id){
        log.info("about to retrieve all instances of a service {}", id);
        List<ServiceInstance> instances = discoveryClient.getInstances(id);
        log.info("got instances {}", instances);
        return instances;
    }
}
