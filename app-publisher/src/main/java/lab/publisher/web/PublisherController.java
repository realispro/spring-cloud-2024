package lab.publisher.web;

import lab.publisher.model.Publisher;
import lab.publisher.service.PublisherService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
public class PublisherController {

    private final PublisherService publisherService;

    @Value("${log.prefix}:'default'")
    private String logPrefix;

    @GetMapping("/publishers")
    List<Publisher> getPublishers(@RequestHeader(value = "custom-header", required = false) String customHeader){
        log.info("[" + logPrefix + "] custom header: {}", customHeader);
        return publisherService.getPublishers();
    }

    @GetMapping("/publishers/{publisherId}")
    ResponseEntity<Publisher> getPublisher(@PathVariable String publisherId, @RequestHeader(value = "custom-header", required = false) String customHeader){
        log.info("[" + logPrefix + "] custom header: {}", customHeader);
        Publisher publisher = publisherService.getPublisher(publisherId);

        return publisher!=null
                ? ResponseEntity.ok(publisher)
                : ResponseEntity.notFound().build();
    }

}
