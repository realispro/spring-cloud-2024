package lab.publisher.service;

import lab.publisher.model.Publisher;
import lab.publisher.repository.PublisherRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Slf4j
@Service
public class PublisherService {

    private final PublisherRepository repository;

    public List<Publisher> getPublishers(){
        log.info("about to retrieve publishers");
        return repository.findAll();
    }

    public Publisher getPublisher(String id){
        log.info("about to retrieve publisher {}", id);
        return repository.findById(id).orElse(null);
    }

    public Publisher addPublisher(Publisher publisher){
        log.info("about to save publisher {}", publisher);
        return repository.save(publisher);
    }

}
