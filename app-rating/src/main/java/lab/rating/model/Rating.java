package lab.rating.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class Rating {

    @Id
    private String id;
    private int bookId;
    private float rate;
    private String review;
    private String commenter;
}
