package lab.rating.service;

import lab.rating.event.RatingCommandEvent;
import lab.rating.repository.RatingRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class RatingListener {

    private final KafkaTemplate<String, RatingUpdate> kafkaTemplate;
    private final RatingRepository ratingRepository;

    @EventListener
    public void handleOrderPlacedEvent(RatingCommandEvent event) {
        log.info("rating update event received: {}", event);
        double avg = ratingRepository.findAllByBookId(event.getBookId()).stream()
                .mapToDouble(rating->rating.getRate())
                .average()
                .orElseThrow();
        kafkaTemplate.send("rating", new RatingUpdate(event.getBookId(), avg));
    }

    @Data
    @RequiredArgsConstructor
    static class RatingUpdate{
        private final int bookId;
        private final double averageRate;
    }
}
