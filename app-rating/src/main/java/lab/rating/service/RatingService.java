package lab.rating.service;

import lab.rating.event.RatingCommandEvent;
import lab.rating.event.RatingCommandType;
import lab.rating.model.Rating;
import lab.rating.repository.RatingRepository;
import lab.rating.service.book.BookClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.util.UUID;


@Service
@RequiredArgsConstructor
@Slf4j
public class RatingService {

    private final RatingRepository repository;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final BookClient bookClient;

    public String addRating(int bookId, float rate, String review) {

        if(bookClient.getBook(bookId)==null){
            throw new RatingException("no book of id " + bookId);
        }

        Rating rating = new Rating();
        //rating.setId(UUID.randomUUID().toString());
        rating.setRate(rate);
        rating.setBookId(bookId);
        rating.setReview(review);

        repository.insert(rating);

        applicationEventPublisher.publishEvent(
                new RatingCommandEvent(this, RatingCommandType.RATING_CREATE, bookId, rate));

        return rating.getId();
    }

    public void correctRating(String ratingId, float rate, String review) {

        Rating rating = repository.findById(ratingId)
                .orElseThrow(() -> new RatingException("invalid rating id " + ratingId));

        rating.setRate(rate);
        rating.setReview(review);
        repository.save(rating);

        applicationEventPublisher.publishEvent(
                new RatingCommandEvent(this, RatingCommandType.RATING_UPDATE, rating.getBookId(), rate));
    }
}
