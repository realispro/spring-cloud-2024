package lab.rating.service.book;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class BookClientFallback implements BookClient{
    @Override
    public BookDTO getBook(int bookId) {
        log.error("handling error on getting book of id {}", bookId);
        return null;
    }
}
