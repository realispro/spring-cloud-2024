/*
package lab.rating.service.book;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
@RequiredArgsConstructor
public class RestBookClient implements BookClient{
    private final RestTemplate restTemplate;

    @Override
    @io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker(name="app-book", fallbackMethod = "getBookByIdFallback")
    public BookDTO getBook(int bookId) {
        log.info("checking existence of a book {}", bookId);

        return restTemplate.exchange(
                "http://app-book" + "/books/" + bookId,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                BookDTO.class).getBody();

    }

    private BookDTO getBookByIdFallback(int bookId, Throwable t){
        log.error("fallback method for id {}", bookId, t);
        return null;
    }

}
*/
