package lab.rating.controller;

import lab.rating.service.RatingException;
import lab.rating.service.RatingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@Slf4j
public class RatingController {

    private final RatingService commandService;

    @PostMapping("/ratings")
    ResponseEntity<?> addRating(@Validated @RequestBody RatingPayload payload, Errors errors){

        if(errors.hasErrors()){
            return ResponseEntity.badRequest().body(errors.getAllErrors());
        }

        String id = commandService.addRating(payload.getBookId(), payload.getRate(), payload.getReview());

        return ResponseEntity.status(HttpStatus.CREATED).body(id);
    }

    @PutMapping("/ratings/{ratingId}")
    ResponseEntity<?> correctRating(@PathVariable String ratingId, @Validated @RequestBody RatingPayload payload, Errors errors){
        if(errors.hasErrors()){
            return ResponseEntity.badRequest().body(errors.getAllErrors());
        }

        commandService.correctRating(ratingId, payload.getRate(), payload.getReview());

        return ResponseEntity.accepted().build();
    }

    @ExceptionHandler(RatingException.class)
    ResponseEntity<String> handeRatingException(RatingException e){
        log.info("rating exception", e);
        return ResponseEntity.badRequest().body(e.getMessage());
    }
}
