package lab.rating.repository;

import lab.rating.model.Rating;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface RatingRepository extends MongoRepository<Rating, String> {

    List<Rating> findAllByBookId(Integer bookId);
}
