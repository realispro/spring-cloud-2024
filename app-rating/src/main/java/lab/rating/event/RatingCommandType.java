package lab.rating.event;

public enum RatingCommandType {

    RATING_CREATE,
    RATING_UPDATE
}
