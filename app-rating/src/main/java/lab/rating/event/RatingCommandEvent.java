package lab.rating.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class RatingCommandEvent extends ApplicationEvent {

    private final RatingCommandType commandType;
    private final int bookId;
    private final float rate;

    public RatingCommandEvent(Object source, RatingCommandType commandType, int bookId, float rate) {
        super(source);
        this.commandType = commandType;
        this.bookId = bookId;
        this.rate = rate;
    }
}
