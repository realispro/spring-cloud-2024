import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LayoutComponent} from './layout/layout.component';
import {LayoutRoutingModule} from './layout-routing.module';
import {DashboardModule} from './dashboard/dashboard.module';
import {PublisherModule} from './publisher/publisher.module';
import {BookModule} from './book/book.module';
import {HeaderComponent} from './layout/template/header/header.component';
import {SidebarComponent} from './layout/template/sidebar/sidebar.component';
import {FooterComponent} from "./layout/template/footer/footer.component";


@NgModule({
  declarations: [LayoutComponent, HeaderComponent, SidebarComponent, FooterComponent],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    DashboardModule,
    PublisherModule,
    BookModule,
  ]
})
export class LayoutModule {
}
