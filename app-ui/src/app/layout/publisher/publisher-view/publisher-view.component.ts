import {Component, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {PublisherService} from '../../../shared/service/publisher.service';
import {takeUntil} from 'rxjs/operators';
import {Location} from '@angular/common';
import {Publisher} from "../../../shared/model/publisher.model";

@Component({
  selector: 'app-department-view',
  templateUrl: './publisher-view.component.html',
  styleUrls: ['./publisher-view.component.css']
})
export class PublisherViewComponent implements OnInit {

  private _$alive = new Subject();
  public publisher: Publisher ;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _location:Location,
    private _publisherService: PublisherService) { }

  ngOnInit(): void {
    this._activatedRoute.params.pipe(takeUntil(this._$alive)).subscribe(params => {
      let id = params['id'];
      this._publisherService.fetchById(id).pipe(takeUntil(this._$alive)).subscribe(result =>{
        this.publisher = result;
      });
    });
  }

  public back(){
    this._location.back();
  }

  public ngOnDestroy(): void {
    this._$alive.next();
    this._$alive.complete();
  }

}
